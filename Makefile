.PHONY: build fmt clean install test

################################################################################

build:
	- mkdir ./bin
	go build -o ./bin/local8 ./cmd/main.go

fmt:
	go fmt ./...

clean:
	- rm -rf ./bin
	- rm -rf .l8
	- rm *.out

install:
	go build -o local8 ./cmd/main.go
	sudo mv local8 /usr/bin

################################################################################

run_test_init:
	go run ./cmd/main.go init -path test -d

run_test_apply:
	go run ./cmd/main.go apply -d

run_version:
	go run ./cmd/main.go version

################################################################################

test:
	go test -coverprofile cover.out ./internal/... -tags test
	go tool cover -func=cover.out

test_verbose:
	go test ./... -v -tags test

test_cover: test
	go tool cover -html=cover.out

