package main

import (
	"bufio"
	"bytes"
	"flag"
	"fmt"
	d "local8/internal/deploy"
	"os"
	"os/exec"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

var commands *flag.FlagSet

func main() {

	commands = flag.NewFlagSet("commands", flag.ExitOnError)
	argPath := commands.String("path", "./", "Sets the dir to look for helmfile and kustomization files")
	argDir := commands.String("dir", ".l8", "Dir to store or read deployment config from")
	debug := commands.Bool("d", false, "Enable debug output")

	if len(os.Args) < 2 {
		printHelp()
	}

	commands.Parse(os.Args[2:])

	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})

	if *debug {
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
		log.Logger = log.With().Caller().Logger()
	} else {
		zerolog.SetGlobalLevel(zerolog.InfoLevel)
		output := zerolog.ConsoleWriter{Out: os.Stdout}
		output.FormatLevel = func(i interface{}) string {
			return ""
		}

		log.Logger = zerolog.New(output).With().Timestamp().Logger()
	}

	deploy := d.New(*argDir, "deploy.yaml", *argPath)

	var err error

	switch os.Args[1] {
	case "init":
		err = deploy.Initialise()
	case "apply":
		err = deploy.Apply()
	case "version":
		fmt.Println("local8\n 0.0.1")
		versionsPrint()

	default:
		commands.PrintDefaults()
	}

	if err != nil {
		log.Fatal().Msg(err.Error())
	}

}

func printHelp() {
	fmt.Println("COMMANDS")
	fmt.Println(" - init: To Initialize the config files")
	fmt.Println(" - apply: To deploy from the config files")
	fmt.Println(" - version: prints version information")
	fmt.Println("OPTIONS")
	commands.PrintDefaults()

	os.Exit(0)
}

func versionsPrint() {

	commandVersion := make(map[string]string)
	commandVersion["kubectl"] = "kubectl version"
	commandVersion["helm"] = "helm version"
	commandVersion["helm diff"] = " helm diff version"
	commandVersion["helmfile"] = "helmfile version"

	for i, kommand := range commandVersion {
		cmd := exec.Command("sh", "-c", kommand)
		var stderr bytes.Buffer
		cmd.Stderr = &stderr
		stdout, err := cmd.StdoutPipe()
		if err != nil {
			log.Fatal().Err(err)
		}
		stdoutBuffer := bufio.NewScanner(stdout)

		fmt.Println(i)

		go func() {
			for stdoutBuffer.Scan() {
				fmt.Println(" ", stdoutBuffer.Text())
			}
		}()
		if err := cmd.Start(); err != nil {
			log.Debug().Err(err).Msg("")

		}
		if err := cmd.Wait(); err != nil {
			if err.Error() == "exit status 1" {
				log.Debug().Err(err).Msg("---")
			} else {
				fmt.Println(" not found")
			}

		}
	}
}
