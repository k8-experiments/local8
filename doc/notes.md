# tools used

- ttyrec
- ttygif
- k9s
- ktx
- k3d
- sublime
- typora

# terminal recording

```bash
SHELL=/usr/bin/sh ttyrec
WINDOWID=$(xdotool getwindowfocus) ttygif
```