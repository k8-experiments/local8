# The deploy file

The deploy file is used to index and configure various deployments. It can be found in `.l8/` folder of where local8 was initialised from. 

```yaml
version: v0
deployments:
- name: one
  path: test/files/helmfile/one
  method: helmfile
  deploy: true
- name: one
  path: test/files/kustomization/one
  method: kustomization
  deploy: false
- name: two
  path: test/files/kustomization/two
  method: kustomization
  deploy: true
  variations:
  - base
  - overlays/prod
  variant: overlays/prod

```



## Tags:

| Tag path                 | Type    | Description                                                  |
| ------------------------ | ------- | ------------------------------------------------------------ |
| version                  | string  | A string used to  describe the version of  the deploy.yaml.  |
| deployments              | string  | An array containing all the deployments.                     |
| deployments.-.name       | string  | The name of the deployment.                                  |
| deployments.-.path       | string  | The path where the deployment files can be found.            |
| deployments.-.method     | string  | Type of deployment (helmfile and kustomiation).              |
| deployments.-.deploy     | boolean | Boolean describing if the the respective apply command should be run for the deployment. |
| deployments.-.variant    | string  | Array of all teh variants found for kustomization            |
| deployments.-.variations | string  | The varaint that should  be applied for a kustomization      |

