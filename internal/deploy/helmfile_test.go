package deploy

import (
	"errors"
	"reflect"
	"testing"
)

func TestGetHelmfileInfo(t *testing.T) {

	type tests struct {
		input  string
		output helmfileInfo
	}

	tst := []tests{{
		input: "testfiles/test/helmfile.yaml",
		output: helmfileInfo{
			name: "test",
			path: "testfiles/test",
		},
	}, {
		input: "test/helmfile.yaml",
		output: helmfileInfo{
			name: "test",
			path: "test",
		},
	},
	}

	for _, j := range tst {
		var testme helmfileInfo
		getHelmfileInfo(&j.input, &testme)
		if testme != j.output {
			printError(t, "getHelmfileInfo", j.output, testme)
		}
	}
}

func TestBuildDeploymentDataHelmfile(t *testing.T) {
	lookup := make(map[string]int)
	var deployments []deployment

	tstInput := []helmfileInfo{{
		name: "testOne",
		path: "testfiles/testOne",
	}, {
		name: "testOne", //duplication test
		path: "testfiles/testOne",
	}, {
		name: "testTwo",
		path: "testTwo",
	},
	}

	tstErr := []error{
		nil, errors.New("Path already indexed"), nil,
	}

	tstOutput := []deployment{{
		Name:       "testOne",
		Path:       "testfiles/testOne",
		Method:     "helmfile",
		Deploy:     true,
		Variations: []string{},
		Variant:    "",
	}, {
		Name:       "testTwo",
		Path:       "testTwo",
		Method:     "helmfile",
		Deploy:     true,
		Variations: []string{},
		Variant:    "",
	}}

	_ = tstOutput

	for i, j := range tstInput {
		err := appendDeploymentHelmfile(&j, &deployments, &lookup)

		if err != nil || tstErr[i] != nil {
			if err.Error() != tstErr[i].Error() {
				printError(t, "buildDeploymentDataHelmfile error", tstErr[i], err)
			}
		}
	}

	for i := range tstOutput {
		if !reflect.DeepEqual(tstOutput[i], deployments[i]) {
			printError(t, "Deployment data mismatch", tstOutput[i], deployments[i])
		}
	}
}
