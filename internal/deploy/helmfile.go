package deploy

import (
	"errors"
	"path"
	"strings"
)

// This function gets the helmfile information from a path and
// stores it in a helmfileInfo struct
// Params
// - filePath *string: pointer to the string contiannign the path string
// - retval *helmfileInfo: pointer to where the struct where the result will be stored
func getHelmfileInfo(filePath *string, retval *helmfileInfo) {
	retval.path = (*filePath)[0:strings.LastIndex((*filePath), "/")]
	retval.name = path.Base(retval.path)
}

// This function is used to append helmfileInfo information to the deployments struct
// Params
// - ki *helmfileInfo: kustomization info to be appended
// - deployments *Deployments: deployments to append to
func appendDeploymentHelmfile(hi *helmfileInfo, deployments *[]deployment, lookup *map[string]int) error {

	_, exists := (*lookup)[hi.path]
	if exists {
		return errors.New("Path already indexed")
	}

	n := deployment{
		Name:       hi.name,
		Path:       hi.path,
		Method:     "helmfile",
		Deploy:     true,
		Variations: []string{},
		Variant:    "",
	}

	*deployments = append(*deployments, n)
	(*lookup)[hi.path] = len(*deployments) - 1

	return nil
}
