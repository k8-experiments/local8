package deploy

type Deploy struct {
	dir        string
	file       string
	path       string
	configPath string
	dc         DeployConfig
	lookup     map[string]int
}

type DeployConfig struct {
	Version     string       `yaml:"version"`
	Deployments []deployment `yaml:"deployments"`
}

type deployment struct {
	Name       string   `yaml:"name,omitempty"`
	Path       string   `yaml:"path,omitempty"`
	Method     string   `yaml:"method,omitempty"`
	Deploy     bool     `yaml:"deploy,omitempty"`
	Variations []string `yaml:"variations,omitempty"`
	Variant    string   `yaml:"variant,omitempty"`
}

type kustomizationInfo struct {
	name    string
	path    string
	variant string
}

type helmfileInfo struct {
	name string
	path string
}
