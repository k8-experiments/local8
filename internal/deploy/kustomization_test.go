package deploy

import (
	"reflect"
	"testing"
)

func TestGetKustomizationInfo(t *testing.T) {

	type tests struct {
		input  string
		output kustomizationInfo
	}

	tst := []tests{
		{
			input: "testfiles/hasura_t/kustomization.yaml",
			output: kustomizationInfo{
				path:    "testfiles/hasura_t",
				variant: "",
				name:    "hasura_t",
			},
		}, {
			input: "testfiles/hasura_x/base/kustomization.yaml",
			output: kustomizationInfo{
				path:    "testfiles/hasura_x",
				variant: "base",
				name:    "hasura_x",
			},
		}, {
			input: "testfiles/hasura_x/overlays/dev/kustomization.yaml",
			output: kustomizationInfo{
				path:    "testfiles/hasura_x",
				variant: "overlays/dev",
				name:    "hasura_x",
			},
		},
	}

	for _, j := range tst {
		var testme kustomizationInfo
		getKustomizationInfo(&j.input, &testme)
		if testme != j.output {
			t.Error("kustomisation info mismatch for", j.input)
		}
	}

}

func TestBuildDeploymentDataKustomization(t *testing.T) {
	lookup := make(map[string]int)
	var deployments []deployment

	tstInput := []kustomizationInfo{{
		path:    "testfiles/hasura_t",
		variant: "",
		name:    "hasura_t",
	}, {
		path:    "testfiles/hasura_x",
		variant: "base",
		name:    "hasura_x",
	}, {
		path:    "testfiles/hasura_x",
		variant: "overlays/dev",
		name:    "hasura_x",
	},
	}

	tstOutput := []deployment{
		{
			Name:       "hasura_t",
			Path:       "testfiles/hasura_t",
			Method:     "kustomization",
			Deploy:     true,
			Variations: []string{},
			Variant:    "",
		}, {
			Name:       "hasura_x",
			Path:       "testfiles/hasura_x",
			Method:     "kustomization",
			Deploy:     true,
			Variations: []string{"base", "overlays/dev"},
			Variant:    "base",
		},
	}

	for _, j := range tstInput {
		buildDeploymentDataKustomization(&j, &deployments, &lookup)
	}

	if len(deployments) != len(tstOutput) {
		printError(t, "Deployment len mistmatch", tstOutput, deployments)
		return
	}

	for i := range deployments {
		if !reflect.DeepEqual(deployments[i], tstOutput[i]) {
			printError(t, "Deployment data mismatch", tstOutput, deployments)
		}
	}

}

func printError(t *testing.T, msg string, expeced interface{}, got interface{}) {
	t.Errorf("msg:%s , expected: %#v got: %#v \n", msg, expeced, got)
}
