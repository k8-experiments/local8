package deploy

import (
	"os"

	"github.com/rs/zerolog/log"
)

// This method is used during the file initialisation phase to
// create the deployment confuguration that is later written to a file
// It is ent to be used with filepath.Walk from the  standard  lib
// Params:
// - filePath string: the path of a file
// - info os.FileInfo: the os.FileInfo interface{}
// - err error: error from filepath.Walk
// Returns:
// - error
func (d *Deploy) filePathHandler(filePath string, info os.FileInfo, err error) error {
	if err != nil {
		return err
	}

	var retval error

	switch info.Name() {

	case "kustomization.yaml":
		var ki kustomizationInfo
		getKustomizationInfo(&filePath, &ki)

		log.Debug().
			Str("variant", ki.variant).
			Str("name", ki.name).
			Str("path", ki.path).
			Msg("--")

		buildDeploymentDataKustomization(&ki, &d.dc.Deployments, &d.lookup)

	case "helmfile.yaml":
		var hi helmfileInfo
		getHelmfileInfo(&filePath, &hi)

		log.Debug().
			Str("name", hi.name).
			Str("path", hi.path).
			Msg("--")

		retval = appendDeploymentHelmfile(&hi, &d.dc.Deployments, &d.lookup)

	}

	return retval
}
