package deploy

import (
	"errors"
	"path"
	"strings"

	"github.com/rs/zerolog/log"
)

// This function gets the kustomization info from a kustomization path and
// stores it in a kustomizationInfo struct
// Params
// - filePath *string: pointer to the string contiannign the path string
// - retval *kustomizationInfo: pointer to where the struct where the result will be stored
func getKustomizationInfo(filePath *string, retval *kustomizationInfo) {

	switch {
	case strings.Contains(*filePath, "base"):
		retval.path = (*filePath)[0 : strings.LastIndex((*filePath), "base")-1]
		retval.variant = "base"
	case strings.Contains((*filePath), "overlays"):
		retval.path = (*filePath)[0 : strings.LastIndex((*filePath), "overlays")-1]
		retval.variant = (*filePath)[strings.LastIndex((*filePath), "overlays"):strings.LastIndexAny((*filePath), "/")]
	default:
		retval.path = (*filePath)[0:strings.LastIndexAny((*filePath), "/")]
	}

	retval.name = path.Base(retval.path)
}

// This function is used to append kustomizationInfo information to the deployments struct
// Params
// - ki *kustomizationInfo: kustomization info to be appended
// - deployments *Deployments: deployments to append to
// - lookup *map[string]int: a lookup table to get the indexes of deployments already added
func buildDeploymentDataKustomization(ki *kustomizationInfo, deployments *[]deployment, lookup *map[string]int) error {

	_, exists := (*lookup)[ki.path]

	if exists {

		index := (*lookup)[ki.path]

		log.Debug().
			Int("index_to_update", index).
			Msg("--- Updating")

		if (*deployments)[index].Method != "kustomization" {
			return errors.New("Path already indexed as" + (*deployments)[index].Method)
		}

		(*deployments)[index].Variations = append(
			(*deployments)[index].Variations,
			ki.variant,
		)

		return nil

	}

	log.Debug().Msg("--- New entry")

	// create the basic ifnformation
	n := deployment{
		Name:       ki.name,
		Path:       ki.path,
		Deploy:     true,
		Method:     "kustomization",
		Variations: []string{},
	}

	// if we do have kustomize variantions such as base and overlay
	// append it and set it to what every we found (ps this is alpahetical)
	if ki.variant != "" {
		n.Variations = append(n.Variations, ki.variant)
		n.Variant = ki.variant
	}

	// append it to the deployments array
	// and create a lookup index so that we can update if we need to
	*deployments = append(*deployments, n)
	(*lookup)[ki.path] = len(*deployments) - 1

	return nil
}
