//go:build !test
// +build !test

package deploy

import (
	"bufio"
	"bytes"
	"errors"
	"io/fs"
	h "local8/internal/helpers"
	"os"
	"os/exec"
	"path"
	"path/filepath"

	"github.com/rs/zerolog/log"
	"gopkg.in/yaml.v2"
)

//Creates a new "instance" of the deploy
// Params:
// - dir string: the dir where deploy configs should be saved
// - file string: the to be used for deploy configs
// - myPath string: the path to search for kustomize
// Returns a deploy struct / object
func New(dir string, file string, myPath string) Deploy {

	dc := DeployConfig{
		Version: "v0",
	}

	d := Deploy{
		dir:        dir,
		file:       file,
		path:       myPath,
		configPath: path.Join(dir, file),
		dc:         dc,
		lookup:     make(map[string]int),
	}

	return d
}

// This method is used to initialise / build the deployment congifuration file
func (d *Deploy) Initialise() error {
	log.Info().
		Str("path", (*d).path).
		Msg("Initialising")

	// create a path just for ourselves
	err := os.Mkdir((*d).dir, 0755)
	if err != nil {
		if !errors.Is(err, fs.ErrExist) {
			return err
		}
	}

	if h.FileExists((*d).configPath) {
		log.Info().Msg("Create new deployment file y/n")
		c, err := h.ConfirmYes(os.Stdin)
		if err != nil {
			return err
		}
		if !c {
			return nil
		}
	}

	// create struct with deployments/ resource configs
	err = filepath.Walk(
		(*d).path,
		(*d).filePathHandler,
	)

	// make yaml string from the data
	data, err := yaml.Marshal(d.dc)
	if err != nil {
		return err
	}

	//write the yaml string to a file
	err = os.WriteFile(path.Join((*d).dir, (*d).file), []byte(data), 0644)
	if err != nil {
		return err
	}

	return nil
}

func (d *Deploy) Apply() error {
	log.Info().
		Msgf("Applying %s", (*d).configPath)

	// read the deploy file and unmarshal
	content, err := os.ReadFile((*d).configPath)
	if err != nil {
		return err
	}

	err = yaml.Unmarshal(content, &d.dc)
	if err != nil {
		return err
	}

	for _, deployment := range (*d).dc.Deployments {
		if deployment.Deploy {

			log.Info().Msgf("Applying (%s) %s", deployment.Method, deployment.Name)

			var kommand string

			switch deployment.Method {
			case "helmfile":
				kommand = "cd " + deployment.Path + " && helmfile --quiet --no-color apply"
			case "kustomization":
				kommand = "pwd; kustomize build " + path.Join(deployment.Path, deployment.Variant) + " | kubectl apply -f -"
			default:
				return nil
			}

			log.Debug().Str("command", kommand).Msg("--")

			cmd := exec.Command("sh", "-c", kommand)

			var stderr bytes.Buffer
			cmd.Stderr = &stderr

			stdout, err := cmd.StdoutPipe()
			if err != nil {
				log.Fatal().Err(err)
			}

			stdoutBuffer := bufio.NewScanner(stdout)
			go func() {
				for stdoutBuffer.Scan() {
					log.Info().
						Str("stdout", stdoutBuffer.Text()).
						Msg("-- " + deployment.Name)
				}
			}()
			if err := cmd.Start(); err != nil {
				return (err)
			}
			if err := cmd.Wait(); err != nil {
				return errors.New(stderr.String())
			}
		}
	}
	return nil
}
