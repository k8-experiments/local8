package deploy

import (
	"errors"
	"os"
	"reflect"
	"testing"
	"time"
)

type fileInfo interface {
	IsDir() bool
	ModTime() time.Time
	Mode() os.FileMode
	Name() string
	Size() int64
	Sys() interface{}
}

type info struct {
	name string
}

func (i info) IsDir() bool        { return true }
func (i info) ModTime() time.Time { return time.Now() }
func (i info) Mode() os.FileMode  { return 0 }
func (i info) Name() string       { return i.name }
func (i info) Size() int64        { return 0 }
func (i info) Sys() interface{}   { return nil }

func TestFilePathHandler(t *testing.T) {
	type testsInput struct {
		filePath string
		info     fileInfo
		err      error
	}

	type testOutput struct {
		dc  DeployConfig
		err error
	}

	tstIn := []testsInput{
		{
			filePath: "testing/kustomization.yaml",
			info:     info{"kustomization.yaml"},
			err:      nil,
		}, {
			filePath: "testing/err.yaml",
			info:     info{"err.yaml"},
			err:      errors.New("myerr"),
		}, {
			filePath: "testingHelm/helmfile.yaml",
			info:     info{"helmfile.yaml"},
			err:      nil,
		},
	}

	tstOut := []testOutput{
		{
			dc: DeployConfig{
				Version: "",
				Deployments: []deployment{{
					Name:       "testing",
					Path:       "testing",
					Method:     "kustomization",
					Deploy:     true,
					Variations: []string{},
					Variant:    "",
				}},
			},
			err: nil,
		}, {
			err: errors.New("myerr"),
		}, {
			dc: DeployConfig{
				Version: "",
				Deployments: []deployment{{
					Name:       "testingHelm",
					Path:       "testingHelm",
					Method:     "helmfile",
					Deploy:     true,
					Variations: []string{},
					Variant:    "",
				}},
			},
			err: nil,
		},
	}

	for i, j := range tstIn {
		deploy := Deploy{
			lookup: map[string]int{},
		}

		err := deploy.filePathHandler(j.filePath, j.info, j.err)

		if err != nil || tstOut[i].err != nil {
			if err.Error() != tstOut[i].err.Error() {
				printError(t, "filePathHandler error not picked up.", err, tstOut[i].err)
			}
		}

		if !reflect.DeepEqual(deploy.dc, tstOut[i].dc) {
			printError(t, "Deployment data mismatch", tstOut[i].dc, deploy.dc)
		}
	}
}
