package helpers

import (
	"testing"
)

func TestFileExists(t *testing.T) {
	if FileExists("noFile.moo") == true {
		t.Fatal("File should not exist!")
	}

	if FileExists("exists_test.go") != true {
		t.Fatal("File should exist!")
	}
}
