package helpers

import (
	"bytes"
	"log"
	"testing"
)

func TestConfirmYes(t *testing.T) {

	{
		content := []byte("yes\n")
		test, err := ConfirmYes(bytes.NewReader(content))
		if err != nil {
			log.Fatal(err)
		}
		if test != true {
			t.Errorf("<yes> should assert to true")
		}
	}

	{
		content := []byte("y\n")
		test, err := ConfirmYes(bytes.NewReader(content))
		if err != nil {
			log.Fatal(err)
		}
		if test != true {
			t.Errorf("<y> should assert to true")
		}
	}

	{
		content := []byte(" Y \n")
		test, err := ConfirmYes(bytes.NewReader(content))
		if err != nil {
			log.Fatal(err)
		}
		if test != true {
			t.Errorf("< Y > should assert to true")
		}
	}

	{
		content := []byte(" a \n")
		test, err := ConfirmYes(bytes.NewReader(content))
		if err != nil {
			log.Fatal(err)
		}
		if test != false {
			t.Errorf("< a > should assert to false")
		}
	}
}
