package helpers

import (
	"bufio"
	"io"
	"strings"
)

// This function is used to validate a yes input from from an io
// Params:
// - input io.Reader: the IO to read from
// Returns:
// - bool: if yes was entered
// - error: if an error was encountered while reading from the io
func ConfirmYes(input io.Reader) (bool, error) {
	reader := bufio.NewReader(input)

	response, err := reader.ReadString('\n')
	if err != nil {
		return false, err
	}

	response = strings.ToLower(strings.TrimSpace(response))

	if response == "y" || response == "yes" {
		return true, nil
	}

	return false, nil
}
