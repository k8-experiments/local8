# Local8

Local8 is a tool that helps with deploying of complex cloud native kubernetes solutions that make use of multiple  helmfiles and kustomization files.

It accomplishes this task by finding all helmfiles and kustomization files (including overlays) in a directory and then indexing them into a deployment configuration file. This deployment configuration can then be used to configure which helmfiles and kustomizations with overlays (including overlays) get deployed by using `local8 apply`

[TOC]

## Demo

### Initialise

Create deployment configuration file of helmfile's and kustomization files

![](doc/demo/init.gif)

### Configure

Disabling one kustomization and changing the overlay of another.

![](doc/demo/config.gif)

### Apply

Apply the the helmfile and the kustomization as per the deployment configuration file.

![](doc/demo/apply.gif)


## Installation

The following is required for installation / building

 - make
 - go 1.16 +

The following is required for running

 - helm 3+
 - helm diff plugin
 - kustomize.io

### Build

To build local8 the following can be run from the root dir of the project.

```bash
make build
```

After there will be an executable binary called `local8` in the root path


### Build and install

To build and install the following can be run from the root dir.

```bash
make install
```
local8 can then be run from anywhere

## Using local8

*local8 makes use of the current folder location for it configuration same as git.*

### 1. Initialise

To get started local8 needs to know of the folders  containing kustomize.yaml and helmfile.yaml files that can be
grouped together for deployment. This is done via initialising local8. It can be done in one of two ways.

#### Kustomize.yaml from current path

```bash
local8 init
```

#### Kustomize.yaml from different path

```bash
local8 init --path <different/path>
```

fter this step has been completed a folder will have been created called `l8` and inside it a deploy.yaml


### 2. Apply

Based on the .k8/deploy.yaml various kustomize.yaml and helfile.yaml  can be deployed to kubernetes with the current kubectl context by running the following command.

```bash
local8 apply
```

### 3. Customise the deployment

Basic customisation can be done within the `l8/deploy` file.

```yaml
- name: hasura
  path: ../resource-configs/hasura
  method: kustomize
  deploy: true
```

For more information please see the documentation in the doc folder
